<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\BlogController;
use App\Http\Controllers\Api\Admin\AuthController;
use App\Http\Controllers\Api\Admin\CommentController;


Route::post('/login', [AuthController::class, 'login']);

//group route with prefix "admin"
Route::prefix('admin')->group(function () {
    //group route with middleware "auth"
    Route::group(['middleware' => 'auth:api'], function() {
        //data user
        Route::get('/user', [AuthController::class, 'getUser']);

        //refresh token JWT
        Route::get('/refresh', [AuthController::class, 'refreshToken']);

        //logout
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::apiResource('/blog', BlogController::class);
        Route::apiResource('/comment', CommentController::class);
    });

});

