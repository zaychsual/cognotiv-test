import { createApp } from 'vue'
import App from './App.vue'

/**
 * import Toastr
 */
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

/**
 * Tailwind CSS
*/
import './style.css'

/**
 * Mixins
 */
import mixins from './mixins';

//create app vue
const app = createApp(App)

//use toast
app.use(Toast)

//use mixins
app.mixin(mixins)

app.mount('#app')
