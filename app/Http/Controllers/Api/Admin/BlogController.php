<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Resources\BlogResource;
use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $index = Blog::when(request()->q, function($search) {
            $search = $search->where('title', 'like', '%'.$search.'%');
        })->latest()->paginate(10);

        return new BlogResource(true, 'List Data', $index);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create tag
        $store = Blog::create([
            'title' => $request->title,
            'slug' => $request->title,
            'content' => $request->content
        ]);

        if($store) {
            //return success with Api Resource
            return new BlogResource(true, 'Data Saved!', $store);
        }

        //return failed with Api Resource
        return new BlogResource(false, 'Failed To Saved', null);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $index = Blog::findOrFail($id);

        if($index) {
            return new BlogResource(true, 'Show Data', $index);
        }
        return new BlogResource(false, 'No Data', null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'     => 'required',
            'content'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //update tag
        $data = Blog::findOrFail($id);
        $data->title = $request->title;
        $data->slug = $request->title;
        $data->content = $request->content;
        $data->update();

        if($data) {
            //return success with Api Resource
            return new BlogResource(true, 'Data Saved!', $data);
        }

        //return failed with Api Resource
        return new BlogResource(false, 'Failed To Save!', null);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $data = Blog::findOrFail($id);
        $data->delete();

        if($data) {
            //return success with Api Resource
            return new BlogResource(true, 'Data Deleted!', null);
        }

        //return failed with Api Resource
        return new BlogResource(false, 'Failed To Delete!', null);
    }
}
