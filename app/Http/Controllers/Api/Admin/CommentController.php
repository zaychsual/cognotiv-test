<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $index = Comment::when(request()->q, function($search) {
            $search = $search->where('title', 'like', '%'.$search.'%');
        })->latest()->paginate(10);

        return new CommentResource(true, 'List Data', $index);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create tag
        $store = Comment::create([
            'blog_id' => $request->blog_id,
            'comment' => $request->comment
        ]);

        if($store) {
            //return success with Api Resource
            return new CommentResource(true, 'Data Saved!', $store);
        }

        //return failed with Api Resource
        return new CommentResource(false, 'Failed To Saved', null);
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $index = Comment::findOrFail($id);

        if($index) {
            return new CommentResource(true, 'Show Data', $index);
        }
        return new CommentResource(false, 'No Data', null);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'comment'     => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //update tag
        $data = Comment::findOrFail($id);
        $data->title = $request->title;
        $data->comment = $request->comment;
        $data->update();

        if($data) {
            //return success with Api Resource
            return new CommentResource(true, 'Data Saved!', $data);
        }

        //return failed with Api Resource
        return new CommentResource(false, 'Failed To Save!', null);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $data = Comment::findOrFail($id);
        $data->delete();

        if($data) {
            //return success with Api Resource
            return new CommentResource(true, 'Data Deleted!', null);
        }

        //return failed with Api Resource
        return new CommentResource(false, 'Failed To Delete!', null);
    }
}
