<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class BaseModel extends \Illuminate\Database\Eloquent\Model
{
    // use HasFactory;
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user              = Auth::user();
            $model->created_by = $user->id;
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();
        });
        static::updating(function ($model) {
            $user              = Auth::user();
            $model->created_by = $user->id;
            $model->updated_at = Carbon::now();
        });
    }
}
